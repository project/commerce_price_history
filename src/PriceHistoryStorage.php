<?php

namespace Drupal\commerce_price_history;

use Drupal\commerce\CommerceContentEntityStorage;
use Drupal\commerce\Context;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_order\PriceCalculatorResult;
use Drupal\commerce_price_history\Entity\PriceHistoryInterface;
use Drupal\commerce_price_history\Exception\NotFoundException;

/**
 * Defines the order storage.
 */
class PriceHistoryStorage extends CommerceContentEntityStorage implements PriceHistoryStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function createByOrderItem(OrderItemInterface $order_item): PriceHistoryInterface {
    $purchasableEntity = $order_item->getPurchasedEntity();
    if (!$purchasableEntity instanceof PurchasableEntityInterface) {
      throw new \InvalidArgumentException('Purchasable entity reference is missing');
    }
    $order = $order_item->getOrder();
    if (!$order instanceof OrderInterface) {
      throw new \InvalidArgumentException('Order reference is missing.');
    }
    return $this->create([
      'entity_type' => $purchasableEntity->getEntityTypeId(),
      'entity_id' => $purchasableEntity->id(),
      'created' => time(),
      'unit_price' => $order_item->getUnitPrice(),
      'adjusted_unit_price' => $order_item->getAdjustedUnitPrice(),
      'adjustments' => $order_item->getAdjustments(),
      'context' => new Context($order->getCustomer(), $order->getStore()),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function createByPurchasableEntityPriceResultAndContext(PurchasableEntityInterface $purchasable_entity, PriceCalculatorResult $price_calculator_result, Context $context): PriceHistoryInterface {
    return $this->create([
      'entity_type' => $purchasable_entity->getEntityTypeId(),
      'entity_id' => $purchasable_entity->id(),
      'created' => time(),
      'unit_price' => $price_calculator_result->getBasePrice(),
      'adjusted_unit_price' => $price_calculator_result->getCalculatedPrice(),
      'adjustments' => $price_calculator_result->getAdjustments(),
      'context' => $context,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function loadLastPrice(PurchasableEntityInterface $entity, Context $context): PriceHistoryInterface {
    // Query all and fetch id of the last one with matching Context.
    $query = $this->getQuery();
    $query->range(0, 1);
    $query->sort('created', 'DESC');
    $query->condition('entity_type', $entity->getEntityTypeId());
    $query->condition('entity_id', $entity->id());
    $query->condition('context.store_id', $context->getStore()->id());
    $query->condition('context.customer_id', $context->getCustomer()->id());
    $query->accessCheck(FALSE);
    $value = $query->execute();
    if (empty($value)) {
      throw new NotFoundException('No price was registered for given criteria');
    }
    $id = reset($value);
    return $this->load($id);
  }

  /**
   * {@inheritdoc}
   */
  public function loadLowestPrice(PurchasableEntityInterface $entity, Context $context, int $interval = 2592000): PriceHistoryInterface {
    // Query all and fetch id of the lowest one with matching Context and
    // within the interval.
    $query = $this->getQuery();
    $query->range(0, 1);
    $query->sort('adjusted_unit_price.number', 'ASC');
    $query->condition('created', time() - $interval, '>');
    $query->condition('entity_type', $entity->getEntityTypeId());
    $query->condition('entity_id', $entity->id());
    $query->condition('context.store_id', $context->getStore()->id());
    $query->condition('context.customer_id', $context->getCustomer()->id());
    $query->accessCheck(FALSE);
    $value = $query->execute();
    if (empty($value)) {
      throw new NotFoundException('No price was registered for given criteria');
    }
    $id = reset($value);
    return $this->load($id);
  }

  /**
   * {@inheritdoc}
   */
  public function loadLowestPriceBeforeCurrent(PurchasableEntityInterface $entity, Context $context, int $interval = 2592000): PriceHistoryInterface {
    $last = $this->loadLastPrice($entity, $context);
    // Query all and fetch id of the lowest one with matching Context, different
    // from last, and within the interval.
    $query = $this->getQuery();
    $query->range(0, 1);
    $query->sort('adjusted_unit_price.number', 'ASC');
    $query->condition('id', $last->id(), '<>');
    $query->condition('created', time() - $interval, '>');
    $query->condition('entity_type', $entity->getEntityTypeId());
    $query->condition('entity_id', $entity->id());
    $query->condition('context.store_id', $context->getStore()->id());
    $query->condition('context.customer_id', $context->getCustomer()->id());
    $query->accessCheck(FALSE);
    $value = $query->execute();
    if (empty($value)) {
      throw new NotFoundException('No price was registered for given criteria');
    }
    $id = reset($value);
    return $this->load($id);
  }

}
