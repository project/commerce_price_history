<?php

namespace Drupal\commerce_price_history\Exception;

/**
 * Exception throw when Price History Entity cannot be found.
 */
class NotFoundException extends \RuntimeException {

}
