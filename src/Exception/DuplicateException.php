<?php

namespace Drupal\commerce_price_history\Exception;

/**
 * Exception throw when duplicate was found.
 */
class DuplicateException extends \LogicException {

}
