<?php

namespace Drupal\commerce_price_history;

use Drupal\commerce\Context;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_cart\Form\AddToCartFormInterface;
use Drupal\commerce_price_history\Exception\DuplicateException;
use Drupal\commerce_price_history\Exception\NotFoundException;
use Drupal\commerce_store\CurrentStoreInterface;
use Drupal\commerce_store\Entity\StoreInterface;
use Drupal\commerce_store\SelectStoreTrait;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Alters the add to cart form for the sake of recording price changes.
 */
class CommercePriceHistoryFormAlter implements ContainerInjectionInterface {

  use SelectStoreTrait;

  /**
   * The constructor of the CommercePriceHistoryFormAlter.
   *
   * @param \Drupal\commerce_store\CurrentStoreInterface $current_store
   *   The current store resolver.
   */
  public function __construct(CurrentStoreInterface $current_store) {
    $this->currentStore = $current_store;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('commerce_store.current_store'));
  }

  /**
   * Takes care of recording the price changes.
   */
  public function formAlter(&$form, FormStateInterface &$form_state) {
    $form_object = $form_state->getFormObject();
    if ($form_object instanceof AddToCartFormInterface) {

      // Get order item from the form.
      /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
      $order_item = $form_object->getEntity();

      // In case the order_item carries outdated entity we must rebuild
      // order_item.
      $selected_variation = $form_state->get('selected_variation');
      if ($selected_variation && $order_item->getPurchasedEntityId() != $selected_variation) {
        $order_item->set('purchased_entity', $selected_variation);
      }

      // The order item must have a valid purchasable entity reference.
      if (!$order_item->getPurchasedEntity() instanceof PurchasableEntityInterface) {
        return;
      }

      $purchased_entity = $order_item->getPurchasedEntity();

      $entityTypeManager = \Drupal::entityTypeManager();
      /** @var \Drupal\commerce_price_history\PriceHistoryStorageInterface $price_history_storage */
      $price_history_storage = $entityTypeManager->getStorage('commerce_price_history');

      // Create entry for the anonymous user.
      // Why anonymous you may ask? Because that limits the scope to something
      // manageable in a short time. But also because that's *probably* what
      // most will be looking for therefore it's a good MVP material. If you
      // believe other types of users should also be covered, feel free
      // to comment on the following issue.
      // @see https://www.drupal.org/project/commerce_price_history/issues/3390977
      try {
        // The order reference is required before the price calculation can be
        // done.
        $store = $this->selectStore($purchased_entity);
        if (!$store instanceof StoreInterface) {
          return;
        }

        /** @var \Drupal\commerce_order\PriceCalculatorInterface $price_calculator */
        $price_calculator = \Drupal::service('commerce_order.price_calculator');
        $context = new Context(User::getAnonymousUser(), $store);
        // As of now we explicitly call for these 2 adjustment types, but this
        // list should be configurable.
        // @see https://www.drupal.org/project/commerce_price_history/issues/3390976
        $calculated_price = $price_calculator->calculate($order_item->getPurchasedEntity(), 1, $context, [
          'tax',
          'promotion',
        ]);
        $price_history = $price_history_storage->createByPurchasableEntityPriceResultAndContext($order_item->getPurchasedEntity(), $calculated_price, $context);

        // Validate if price needs to be recorded.
        // When creating new entry check if different from the last one.
        try {
          $last_price = $price_history_storage->loadLastPrice($purchased_entity, $context);
          if (!$price_history->getUnitPrice()->compareTo($last_price->getUnitPrice()) && !$price_history->getAdjustedUnitPrice()->compareTo($last_price->getAdjustedUnitPrice())) {
            throw new DuplicateException('Neither unit price nor adjusted unit price did change');
          }
        }
        catch (NotFoundException $exception) {
          // If this exception is thrown it typically means the price for this
          // purchasable entity-context combo hasn't been tracked yet.
        }
        catch (DuplicateException $exception) {
          throw $exception;
        }

        $price_history->save();
      }
      catch (DuplicateException $exception) {
        // Duplicates are discovered in preSave. If duplicate was found this
        // exception is thrown, but we don't have to react to it as this is an
        // expected outcome.
      }
      catch (\Exception $exception) {
        if (!$exception->getPrevious() instanceof DuplicateException) {
          \Drupal::logger('commerce_price_history')
            ->warning('The price history entity could not be created. Reason: @message',
              ['@message' => $exception->getMessage()]);
        }
      }
    }
  }

}
