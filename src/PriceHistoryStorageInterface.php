<?php

namespace Drupal\commerce_price_history;

use Drupal\commerce\Context;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_order\PriceCalculatorResult;
use Drupal\commerce_price_history\Entity\PriceHistoryInterface;
use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Defines the interface for order storage.
 */
interface PriceHistoryStorageInterface extends ContentEntityStorageInterface {

  /**
   * Creates new Price History entity by order item.
   *
   * The entity will only be created when the price is different from the last
   * price recorded for the purchasable entity.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
   *   The order item interface.
   *
   * @return \Drupal\commerce_price_history\Entity\PriceHistoryInterface
   *   The created price history.
   */
  public function createByOrderItem(OrderItemInterface $order_item): PriceHistoryInterface;

  /**
   * Creates new Price History entity by price calculator result and context.
   *
   * The entity will only be created when the price is different from the last
   * price recorded for the purchasable entity.
   *
   * @param \Drupal\commerce\PurchasableEntityInterface $purchasable_entity
   *   The purchasable entity.
   * @param \Drupal\commerce_order\PriceCalculatorResult $price_calculator_result
   *   The price calculator result.
   * @param \Drupal\commerce\Context $context
   *   The commerce context.
   *
   * @return \Drupal\commerce_price_history\Entity\PriceHistoryInterface
   *   The created price history.
   */
  public function createByPurchasableEntityPriceResultAndContext(PurchasableEntityInterface $purchasable_entity, PriceCalculatorResult $price_calculator_result, Context $context): PriceHistoryInterface;

  /**
   * Loads the last price for the purchasable entity.
   *
   * @param \Drupal\commerce\PurchasableEntityInterface $entity
   *   The purchasable entity.
   * @param \Drupal\commerce\Context $context
   *   The commerce context.
   *
   * @return \Drupal\commerce_price_history\Entity\PriceHistoryInterface
   *   the price history entity.
   */
  public function loadLastPrice(PurchasableEntityInterface $entity, Context $context): PriceHistoryInterface;

  /**
   * Load the lowest price for the purchasable entity within the interval.
   *
   * @param \Drupal\commerce\PurchasableEntityInterface $entity
   *   The purchasable entity.
   * @param \Drupal\commerce\Context $context
   *   The commerce context.
   * @param int $interval
   *   The interval, default to 30 days.
   *
   * @return \Drupal\commerce_price_history\Entity\PriceHistoryInterface
   *   The price history entity.
   */
  public function loadLowestPrice(PurchasableEntityInterface $entity, Context $context, int $interval = 2592000): PriceHistoryInterface;

  /**
   * Load the lowest price for the purchasable entity within the interval.
   *
   * The current price is ignored.
   *
   * @param \Drupal\commerce\PurchasableEntityInterface $entity
   *   The purchasable entity.
   * @param \Drupal\commerce\Context $context
   *   The commerce context.
   * @param int $interval
   *   The interval, default to 30 days.
   *
   * @return \Drupal\commerce_price_history\Entity\PriceHistoryInterface
   *   The price history entity.
   */
  public function loadLowestPriceBeforeCurrent(PurchasableEntityInterface $entity, Context $context, int $interval = 2592000): PriceHistoryInterface;

}
