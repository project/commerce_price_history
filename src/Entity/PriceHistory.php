<?php

namespace Drupal\commerce_price_history\Entity;

use Drupal\commerce\Context;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_price\Price;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the commerce price history entity class.
 *
 * @ContentEntityType(
 *   id = "commerce_price_history",
 *   label = @Translation("Commerce price history"),
 *   label_collection = @Translation("Commerce price histories"),
 *   label_singular = @Translation("commerce price history"),
 *   label_plural = @Translation("commerce price histories"),
 *   label_count = @PluralTranslation(
 *     singular = "@count commerce price histories",
 *     plural = "@count commerce price histories",
 *   ),
 *   handlers = {
 *     "storage" = "Drupal\commerce_price_history\PriceHistoryStorage",
 *     "list_builder" = "Drupal\commerce_price_history\CommercePriceHistoryListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\commerce_price_history\Routing\CommercePriceHistoryHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "commerce_price_history",
 *   admin_permission = "administer commerce price history",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *   },
 *   links = {
 *     "canonical" = "/commerce-price-history/{commerce_price_history}",
 *     "delete-form" = "/commerce-price-history/{commerce_price_history}/delete",
 *   },
 * )
 */
class PriceHistory extends ContentEntityBase implements PriceHistoryInterface {

  /**
   * {@inheritdoc}
   */
  public function getPurchasedEntity(): PurchasableEntityInterface {
    return \Drupal::entityTypeManager()
      ->getStorage($this->get('entity_type')->value)
      ->load($this->get('entity_id')->target_id);

  }

  /**
   * {@inheritdoc}
   */
  public function getContext(): Context {
    return $this->get('context')->first()->toContext();
  }

  /**
   * {@inheritdoc}
   */
  public function getUnitPrice(): Price {
    return $this->get('unit_price')->first()->toPrice();
  }

  /**
   * {@inheritdoc}
   */
  public function getAdjustedUnitPrice(): Price {
    return $this->get('adjusted_unit_price')->first()->toPrice();
  }

  /**
   * {@inheritdoc}
   */
  public function getAdjustments(array $adjustment_types = []): array {
    /** @var \Drupal\commerce_order\Adjustment[] $adjustments */
    $adjustments = $this->get('adjustments')->getAdjustments();
    // Filter adjustments by type, if needed.
    if ($adjustment_types) {
      foreach ($adjustments as $index => $adjustment) {
        if (!in_array($adjustment->getType(), $adjustment_types)) {
          unset($adjustments[$index]);
        }
      }
      $adjustments = array_values($adjustments);
    }

    return $adjustments;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['entity_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity type'))
      ->setRequired(TRUE)
      ->setDescription(t('The entity type to which this entry is attached.'))
      ->setSetting('is_ascii', TRUE)
      ->setSetting('max_length', EntityTypeInterface::ID_MAX_LENGTH);

    $fields['entity_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Entity ID'))
      ->setDescription(t('The ID of the entity to which this entry is attached.'))
      ->setRequired(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the commerce price history entry was created.'))
      ->setReadOnly(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['unit_price'] = BaseFieldDefinition::create('commerce_price')
      ->setLabel(t('The resolved price.'))
      ->setDescription(t('The price before adjustments'))
      ->setReadOnly(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['adjusted_unit_price'] = BaseFieldDefinition::create('commerce_price')
      ->setLabel(t('The calculated price.'))
      ->setDescription(t('The price after adjustments'))
      ->setReadOnly(TRUE)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['adjustments'] = BaseFieldDefinition::create('commerce_adjustment')
      ->setLabel(t('Adjustments'))
      ->setRequired(FALSE)
      ->setReadOnly(TRUE)
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['context'] = BaseFieldDefinition::create('commerce_price_history_context')
      ->setLabel(t('Context'))
      ->setDescription(t('A serialized context object.'));

    return $fields;
  }

}
