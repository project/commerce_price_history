<?php

namespace Drupal\commerce_price_history\Entity;

use Drupal\commerce\Context;
use Drupal\commerce\PurchasableEntityInterface;
use Drupal\commerce_price\Price;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface defining a commerce price history entity type.
 */
interface PriceHistoryInterface extends ContentEntityInterface {

  /**
   * The purchased entity.
   *
   * @return \Drupal\commerce\PurchasableEntityInterface
   *   The purchasable entity.
   */
  public function getPurchasedEntity(): PurchasableEntityInterface;

  /**
   * The unit price.
   *
   * @return \Drupal\commerce_price\Price
   *   The price value object.
   */
  public function getUnitPrice(): Price;

  /**
   * The adjusted unit price.
   *
   * @return \Drupal\commerce_price\Price
   *   The price value object.
   */
  public function getAdjustedUnitPrice(): Price;

  /**
   * List of adjustments.
   *
   * @param array $adjustment_types
   *   Adjustment types to return.
   *
   * @return \Drupal\commerce_order\Adjustment[]
   *   The array of adjustments.
   */
  public function getAdjustments(array $adjustment_types = []): array;

  /**
   * The Commerce context value object.
   *
   * @return \Drupal\commerce\Context
   *   The context value object.
   */
  public function getContext(): Context;

}
