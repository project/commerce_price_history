<?php

namespace Drupal\commerce_price_history;

use Drupal\commerce\Context;
use Drupal\commerce_store\CurrentStoreInterface;
use Drupal\commerce_store\SelectStoreTrait;
use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\user\Entity\User;

/**
 * The computed field which exposes the lowest price.
 */
final class ComputedLowestPrice extends FieldItemList {

  use ComputedItemListTrait;
  use SelectStoreTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    if (!$this->currentStore instanceof CurrentStoreInterface) {
      $this->currentStore = \Drupal::service('commerce_store.current_store');
    }
    /** @var \Drupal\commerce\PurchasableEntityInterface $purchased_entity */
    $purchased_entity = $this->getEntity();

    /** @var \Drupal\commerce_price_history\PriceHistoryStorageInterface $price_history_storage */
    $price_history_storage = \Drupal::entityTypeManager()->getStorage('commerce_price_history');
    $price = NULL;

    try {
      $context = new Context(User::getAnonymousUser(), $this->selectStore($purchased_entity));
      $price = $price_history_storage->loadLowestPrice($purchased_entity, $context)->getAdjustedUnitPrice();
    }
    catch (\Throwable $throwable) {

    }

    $this->list[0] = $this->createItem(0, $price);
  }

}
