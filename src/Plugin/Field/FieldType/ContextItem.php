<?php

namespace Drupal\commerce_price_history\Plugin\Field\FieldType;

use Drupal\commerce\Context;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataReferenceTargetDefinition;

/**
 * Plugin implementation of the 'commerce_price_history_context' field type.
 *
 * @FieldType(
 *   id = "commerce_price_history_context",
 *   label = @Translation("Context"),
 *   description = @Translation("Stores commerce context."),
 *   category = @Translation("Commerce"),
 *   no_ui = TRUE,
 * )
 */
class ContextItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['store_id'] = DataReferenceTargetDefinition::create('integer')
      ->setLabel(t('Store ID'))
      ->setSetting('unsigned', TRUE)
      ->setRequired(TRUE);
    $properties['customer_id'] = DataReferenceTargetDefinition::create('integer')
      ->setLabel(t('Customer ID'))
      ->setSetting('unsigned', TRUE)
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'store_id' => [
          'description' => 'The store ID.',
          'type' => 'int',
          'unsigned' => TRUE,
        ],
        'customer_id' => [
          'description' => 'The customer ID.',
          'type' => 'int',
          'unsigned' => TRUE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $entity_type_manager = \Drupal::entityTypeManager();
    /** @var \Drupal\commerce_store\Entity\StoreInterface[] $currencies */
    $stores = $entity_type_manager->getStorage('commerce_store')->loadMultiple();
    $store_id = reset($stores)->id();

    return [
      'store_id' => $store_id,
      // Use anonymous user.
      'customer_id' => 0,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return $this->store_id === NULL || $this->customer_id === NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    // Allow callers to pass a Price value object as the field item value.
    if ($values instanceof Context) {
      $values = [
        'store_id' => $values->getStore()->id(),
        'customer_id' => $values->getCustomer()->id(),
      ];
    }
    parent::setValue($values, $notify);
  }

  /**
   * Gets the Context value object for the current field item.
   *
   * @return \Drupal\commerce\Context
   *   The Context object.
   */
  public function toContext(): Context {
    $entity_type_manager = \Drupal::entityTypeManager();
    $store_storage = $entity_type_manager->getStorage('commerce_store');
    $user_storage = $entity_type_manager->getStorage('user');
    return new Context($user_storage->load($this->customer_id), $store_storage->load($this->store_id));
  }

}
